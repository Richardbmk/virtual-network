locals {
  assetname = "rick"
  environment = "example"
  location = "westus"

  resource_name = format("%s-%s-%s", local.assetname, local.environment, local.location)
}

resource "azurerm_resource_group" "resourcegroup" {
  name = "${local.resource_name}-rg-1"
  location = local.location
}


module "virtual_network" {
  source = "git::https://gitlab.com/Richardbmk/virtual-network.git?ref=v1.0.1"

  instance_count          = 1
  vnet_name               = "${local.resource_name}-vnet-${local.environment}"
  address_space           = var.address_space
  address_prefixes        = var.address_prefixes
  public_ip_name          = "mypublicip-1-${local.environment}"
  subnet_name             = "mysubnet-1-${local.environment}"
  random_info             = local.random_info
  resource_group_name     = azurerm_resource_group.resourcegroup.name
  resource_group_location = azurerm_resource_group.resourcegroup.location
  environment             = local.environment
}
