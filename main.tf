# Create Virtual Network
resource "azurerm_virtual_network" "myvnet" {
  name = var.vnet_name

  address_space       = var.address_space
  location            = var.resource_group_location
  resource_group_name = var.resource_group_name
}

# Create Subnet
resource "azurerm_subnet" "mysubnet" {
  count = var.instance_count
  name  = "${var.subnet_name}-${count.index}"

  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.myvnet.name
  address_prefixes     = var.address_prefixes
}

# Create Public IP Address
resource "azurerm_public_ip" "mypublicip" {
  name = var.public_ip_name

  resource_group_name = var.resource_group_name
  location            = var.resource_group_location
  allocation_method   = "Static"
  domain_name_label   = "app1-vm-${var.random_info}"
  tags = {
    environment = var.environment
  }
}
