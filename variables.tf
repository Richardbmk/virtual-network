
variable "vnet_name" {
  type = string
}

variable "address_space" {
  type = list(any)
}

variable "resource_group_name" {
  type = string
}

variable "resource_group_location" {
  type = string
}

variable "address_prefixes" {
  type = list(any)
}

variable "public_ip_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "random_info" {
  type = string
}

variable "subnet_name" {
  type = string
}

variable "instance_count" {
  type    = number
  default = 1
}
